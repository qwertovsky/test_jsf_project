package org.qwertovsky.test.jsf.controller;

public class Member {
	private String name;
	
	public Member(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
