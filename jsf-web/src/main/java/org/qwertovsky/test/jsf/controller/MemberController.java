package org.qwertovsky.test.jsf.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name="memberController")
public class MemberController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Member> members = new ArrayList<Member>();
	
	private String name;
	
	public void addMember() {
		Member m = new Member(name);
		System.out.println(this);
		System.out.println(m);
		members.add(m);
		name = "";
	}
	
	public List<Member> getMembers() {
		return this.members;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}