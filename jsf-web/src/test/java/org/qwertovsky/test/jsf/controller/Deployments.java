package org.qwertovsky.test.jsf.controller;

import java.io.File;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.Testable;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;

public class Deployments {
	
	
	@Deployment(name="client", testable=false, managed=false)
	public static EnterpriseArchive createDeployment() {
		EnterpriseArchive ear = ShrinkWrap.createFromZipFile(EnterpriseArchive.class
				, new File("../jsf-ear/target/jsf-ear.ear"));
		return ear;
	}
	
	@Deployment(name="container")
	public static EnterpriseArchive createTestableDeployment(Class<?> clazz) {
		EnterpriseArchive ear = ShrinkWrap.createFromZipFile(EnterpriseArchive.class
				, new File("../jsf-ear/target/jsf-ear.ear"));
		WebArchive jsfWebWar = (WebArchive) ear.getAsType(WebArchive.class, "jsf-web.war");
		jsfWebWar.addClass(clazz);
		Testable.archiveToTest(jsfWebWar);
//		System.out.println(jsfWebWar.toString(true));
		return ear;
	}

}
