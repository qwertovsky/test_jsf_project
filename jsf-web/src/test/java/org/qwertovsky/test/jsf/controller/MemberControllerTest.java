package org.qwertovsky.test.jsf.controller;

import javax.faces.bean.ManagedProperty;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class MemberControllerTest {
	
	@ManagedProperty("#{memberController}")
	public MemberController memberController; //NULL
	
	@Deployment
	public static EnterpriseArchive createDeployment() {
		return Deployments.createTestableDeployment(MemberControllerTest.class);
	}
	
	@Test
	@Ignore
	@InSequence(1)
	public void testAddMember() {
		// NPE
		Assert.assertEquals(0, memberController.getMembers().size());
		memberController.addMember();
		Assert.assertEquals(1, memberController.getMembers().size());
	}
	
}
