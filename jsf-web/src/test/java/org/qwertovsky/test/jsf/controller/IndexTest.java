package org.qwertovsky.test.jsf.controller;

import java.net.MalformedURLException;
import java.net.URL;

import javax.faces.bean.ManagedProperty;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.Graphene;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.warp.Activity;
import org.jboss.arquillian.warp.Inspection;
import org.jboss.arquillian.warp.Warp;
import org.jboss.arquillian.warp.WarpTest;
import org.jboss.arquillian.warp.jsf.AfterPhase;
import org.jboss.arquillian.warp.jsf.BeforePhase;
import org.jboss.arquillian.warp.jsf.Phase;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@RunWith(Arquillian.class)
@WarpTest
@RunAsClient
public class IndexTest {
	
	@Drone
    private WebDriver browser;
	@ArquillianResource
    private URL deploymentUrl;
	
	@FindBy(id="form:member_name")                                     
	private WebElement memberName;
	@FindBy(id="form:add_member_btn")
	private WebElement memberAddBtn;
	private WebElement listSize;
	
	@Deployment(name="client")
	public static EnterpriseArchive createDeployment() {
		return Deployments.createDeployment();
	}
	
	
//	@Test
//	@InSequence(1)
//	@RunAsClient
//	@OperateOnDeployment("client")
//	public void testAddMember1() throws MalformedURLException {
//		browser.get(deploymentUrl.toExternalForm() + "index.xhtml");
//		
//		memberName.sendKeys("test1");
//		memberAddBtn.click();
//		Graphene.waitAjax().until().element(By.id("form:list_size_message")).is().present();
//		listSize = browser.findElement(By.id("form:list_size_message"));
//		long size = Long.valueOf(listSize.getText());
//		Assert.assertEquals(1, size);
//	}
//	
//	@Test
//	@InSequence(2)
//	@RunAsClient
//	@OperateOnDeployment("client")
//	public void testAddMember2() throws MalformedURLException {
//		browser.get(deploymentUrl.toExternalForm() + "index.xhtml");
//		
//		memberName.sendKeys("test2");
//		Graphene.guardAjax(memberAddBtn).click();
//		listSize = browser.findElement(By.id("form:list_size_message"));
//		long size = Long.valueOf(listSize.getText());
//		Assert.assertEquals(2, size);
//	}
	
	@Test
	@InSequence(1)
	public void testAddFirstMembers() {
		
		Warp.initiate(new Activity() {
			@Override
			public void perform() {
				browser.get(deploymentUrl.toExternalForm() + "index.xhtml");
				memberName.sendKeys("test1");
				memberAddBtn.click();
				Graphene.waitAjax().until().element(By.id("form:list_size_message")).is().present();
				listSize = browser.findElement(By.id("form:list_size_message"));
				long size = Long.valueOf(listSize.getText());
				Assert.assertEquals(1, size);
				
				memberName.sendKeys("test2");
				Graphene.guardAjax(memberAddBtn).click();
				listSize = browser.findElement(By.id("form:list_size_message"));
				size = Long.valueOf(listSize.getText());
				Assert.assertEquals(2, size);
			}
		}).inspect(new Inspection() {
			private static final long serialVersionUID = 1L;
		});
		
	}
	
	@Test
	@InSequence(3)
	public void testAddMember3() {
		final String name = "test3";
		
		Warp.initiate(new Activity() {
			@Override
			public void perform() {
				memberName.sendKeys(name);
				memberAddBtn.click();
			}
		})
		.inspect(new Inspection() {
			
			private static final long serialVersionUID = 1L;
			
			@ManagedProperty("#{memberController}")
			MemberController memberController;
			
			@BeforePhase(Phase.UPDATE_MODEL_VALUES)
			public void inspectTwoInList() {
				Assert.assertEquals(2, memberController.getMembers().size());
			}
			
			@AfterPhase(Phase.RENDER_RESPONSE)
			public void inspectThreeInList() {
				Assert.assertEquals(3, memberController.getMembers().size());
				Assert.assertEquals(name, memberController.getMembers().get(2).getName());
			}
		});
	}

}
